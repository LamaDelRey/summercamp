package com.example.demoapp;

import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.demoapp.R;
import com.tagby.sdk.app.TagByApplication;
import com.tagby.sdk.app.TagByDocumentFragment;
import com.tagby.sdk.app.TagByOnClickListener;
import com.tagby.sdk.app.controllers.DocumentBackgroundController;
import com.tagby.sdk.app.controllers.TagByButtonController;
import com.tagby.sdk.app.controllers.TagByLabelController;
import com.tagby.sdk.app.controllers.TagByProgressImageController;
import com.tagby.sdk.simpleapp.SimpleTagByActivity;
import com.tagby.sdk.ui.NetworkImageProgressView;
import com.tagby.sdk.utils.Check;

/**
 * This is the main application fragment responsible for rendering application specific main screen.
 * 
 * The contents of this fragment are either bundled within the app or configured on via Tag'By Manager
 * using offer designer.
 */
public class MyMainFragment extends TagByDocumentFragment<MyDocument>{
	// we need to keep the reference to these controls
	// as we would like to hide / show it based on the application state
	private View mSwitchDocument;
	private Button mShareButton;
	
	public static MyMainFragment newInstance (MyDocument document) {
		Check.Argument.isNotNull(document, "document");
	
		MyMainFragment fragment = new MyMainFragment();

		fragment.setArguments(fragment.createBundle(document));
		
		return fragment;
	}
	
	/**
	 * In this method we need to setup the fragment based on the selected offer. 
	 */
	@Override
	protected View safelyOnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) throws Exception {
		Log.d(TAG, "safelyOnCreateView");
		
		// retrieve the current offer
		final MyDocument document = getDocument();
		
		WindowManager wm = (WindowManager) TagByApplication.getInstance().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		
		View rootView = inflater.inflate(R.layout.fragment_main, container, false);
		NetworkImageProgressView logoView = (NetworkImageProgressView)rootView.findViewById(R.id.fragment_main_logo);
		NetworkImageProgressView imageView = (NetworkImageProgressView)rootView.findViewById(R.id.fragment_main_image);
		TextView labelView = (TextView)rootView.findViewById(R.id.fragment_main_label);
		mSwitchDocument = rootView.findViewById(R.id.fragment_main_switch_document);
		mShareButton = (Button)rootView.findViewById(R.id.fragment_main_share);
		
		// the following lines configure fragment widgets as designed in the offer
		// do not forget to call 'apply' for every newly created controller
		DocumentBackgroundController.Instance.apply(rootView, document.getBackground(), metrics);
		new TagByButtonController(mShareButton, document.getShareButton()).apply();
		new TagByProgressImageController(logoView, document.getLogo()).apply();
		new TagByProgressImageController(imageView, document.getImage()).apply();
		new TagByLabelController(labelView, document.getInfoLabel()).apply();
		
		mSwitchDocument.setOnClickListener(
				new TagByOnClickListener() {				
					@Override
					protected void safelyOnClick(View v) throws Exception {
						setControls (false);
						
						((SimpleTagByActivity<?>)getActivity()).onDocumentSwitch();
					}
				});
		
		mShareButton.setOnClickListener(
			new TagByOnClickListener() {				
				@Override
				protected void safelyOnClick(View v) throws Exception {
					setControls (false);
					
					((SimpleTagByActivity<?>)getActivity()).onShare();
				}
			});
		
		return rootView;
	}
	
	/*
	 * Called by the {@link MainActivity} 
	 */
	public void restart() {
		setControls (true);
	}
	
	private void setControls(boolean enabled) {
		mSwitchDocument.setVisibility(enabled ? View.VISIBLE : View.GONE);
		mShareButton.setVisibility(enabled ? View.VISIBLE : View.GONE);
	}
}