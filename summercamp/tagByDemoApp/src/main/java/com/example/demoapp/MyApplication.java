package com.example.demoapp;

import java.util.ArrayList;
import java.util.List;

import com.tagby.sdk.app.TagByApplication;
import com.tagby.sdk.app.TagByApplicationDocuments;
import com.tagby.sdk.documents.TagByDocument;

public class MyApplication extends TagByApplication<MyDocument>{
	/**
	 * This method will be called by Tag'By runtime components after the offers are fetched from the server.
	 */
	@Override
	protected TagByApplicationDocuments<MyDocument> createDocuments(List<TagByDocument> documents) throws Exception {
		List<MyDocument> myDocuments = new ArrayList<MyDocument>(documents.size());
		 
		for(TagByDocument document: documents) {
			// we don't want to display the offers that have no social network configured
			// if your application doesn't work well with all social network providers
			// you must take it into account here
			if (document.getSocialNetworks().anySupported() == false)
				continue;
			
			myDocuments.add(
				new MyDocument (document.getData()));
		}
		
		// additionally we get rid of any NFC enabled offers if the application is run on non-NFC device
		myDocuments = super.filterOutNfcEnabled(myDocuments);
		
		return new TagByApplicationDocuments<MyDocument> (myDocuments);
	}
}