package com.example.demoapp;

//import com.example.demoapp.R;

import com.tagby.sdk.api.PublishCommand;
import com.tagby.sdk.app.MessagePresenter;
import com.tagby.sdk.app.TagByApplication;
import com.tagby.sdk.app.TagByAsyncTask;
import com.tagby.sdk.resources.ResourceUtils;
import com.tagby.sdk.simpleapp.SimpleTagByActivity;
import com.tagby.sdk.simpleapp.fragments.MessageFragment;

import android.app.FragmentManager;
import android.os.Handler;
import android.util.Log;

public class MainActivity extends SimpleTagByActivity<MyDocument> {
	private final static String TAG_MAIN_FRAGMENT = "mainFragment";
	private final static String TAG_MESSAGE = "message";
	
	private final Handler mHandler = new Handler();
	
	/**
	 * This is the main entry point after the document has been selected.
	 * It is the application responsibility to present document UI.
	 */
	@Override
	public void onDocumentSelected(MyDocument document) throws Exception {
		super.onDocumentSelected(document);
		
		final FragmentManager fm = getFragmentManager();
		fm.beginTransaction()
		.replace(R.id.main_frame, MyMainFragment.newInstance(document), TAG_MAIN_FRAGMENT)
		.addToBackStack(TAG_MAIN_FRAGMENT)
		.commit();
	}
	
	/**
	 * Called by the base class after share now screen has been closed.
	 * Depending on the application type it could be required to update some UI here.
	 * For this demo application we restore the document switch and share buttons. 
	 */
	@Override
	public void onShareClosed() throws Exception {
		super.onShareClosed();
		
		MyMainFragment fragment = (MyMainFragment)getFragmentManager().findFragmentByTag(TAG_MAIN_FRAGMENT);
		if (fragment != null) {
			fragment.restart();
		}
	}
	
	/**
	 * Called by the base class after the user has identified himself and his friends and decides to share.
	 */
	@Override
	public void onShareNow() throws Exception {
		Log.d(TAG, "onShareNow");
		
		super.onShareNow();
		
		// we call the base class method in order
		// to go back to the main fragment screen
		super.onShareClosed();
		
		new PublishTask().execute();
	}
	
	private void closeSharedMessage () throws Exception {
		// close "Sharing now..." message
		getFragmentManager().popBackStack();
		((MyMainFragment)getFragmentManager().findFragmentByTag(TAG_MAIN_FRAGMENT)).restart();
	}
	
	@Override
	protected void safelyOnBackPressed() throws Exception {
		Log.d(TAG, "safelyOnBackPressed");
		
		// we would like to block BACK button as far as the message button is displayed
		FragmentManager fm = getFragmentManager();
		if (fm.getBackStackEntryCount() != 0) {
			if (fm.findFragmentByTag(TAG_MESSAGE) != null)
				return;
		}
		
		super.safelyOnBackPressed();
	}
	
	/**
	 * Publishes the message.
	 */
	private class PublishTask extends TagByAsyncTask<Void, Void, Void> {
		protected PublishTask() {
			super(MainActivity.this, ResourceUtils.publishing(MainActivity.this), true);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			getFragmentManager()
			.beginTransaction()
			.add(R.id.main_frame, MessageFragment.newInstance(getString(R.string.sharing_message)), TAG_MESSAGE)
			.addToBackStack(null)
			.commit();
		}
		
		@Override
		protected Void safelyDoInBackground(Void... params) throws Exception {
			new PublishCommand(
				MainActivity.this.getRemoteDeviceId(),
				MainActivity.this.getOfferId(),
				null,
				null,
				null,
				TagByApplication.getInstance().getUsers().getUsers()).execute();
			
			return null;
		}
		
		@Override
		protected void onFailure(Exception exception) {
			super.onFailure(exception);
			
			try {
				// close "Sharing now..." message
				getFragmentManager().popBackStack();
				((MyMainFragment)getFragmentManager().findFragmentByTag(TAG_MAIN_FRAGMENT)).restart();
			} catch (Exception e) {
				MessagePresenter.getInstance().showException(e);
			}
		}
		
		@Override
		protected void onSuccess(Void unused) {
			// replace "Sharing now..." message with "Thank you"
			((MessageFragment)getFragmentManager().findFragmentByTag(TAG_MESSAGE)).setMessage(getString(R.string.shared_message));
			
			mHandler.postDelayed(
				new Runnable() {
					@Override
					public void run() {
						try {
							closeSharedMessage();
						}
						catch (Exception exception) {
							MessagePresenter.getInstance().showException(exception);
						}
					}
				}, 3000);
		}
	}
}