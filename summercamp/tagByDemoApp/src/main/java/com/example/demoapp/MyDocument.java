package com.example.demoapp;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

import com.tagby.sdk.documents.TagByDocument;
import com.tagby.sdk.documents.widgets.json.TagByButton;
import com.tagby.sdk.documents.widgets.json.TagByImage;
import com.tagby.sdk.documents.widgets.json.TagByLabel;
import com.tagby.sdk.exceptions.TagByException;

/**
 * This is the class that represents your document / offer configuration.
 * <p>
 * You should put here everything that comes from the server and it is configured on the web portal.
 * It includes UI widgets but also the non-UI settings that are used to change your application
 * behavior.
 * </p>
 * <p>
 * Additionally in order to meet {@link Parcelable} requirements, some additional
 * members like constructor must be implemented.  
 * </p>
 * @author Rafal Grzybowski
 */
public class MyDocument extends TagByDocument {

	public final static String WIDGET_LOGO = "logo-image";
	public final static String WIDGET_IMAGE = "custom-image";
	public final static String WIDGET_INFO_LABEL = "info-label";
	public final static String WIDGET_SHARE_BUTTON = "share-button";
	
	public MyDocument(JSONObject data) throws JSONException, TagByException {
		super(data);
	}

	public MyDocument(Parcel parcel) throws JSONException, TagByException {
		this(new JSONObject(parcel.readString()));
	}
	
	public static final Parcelable.Creator<MyDocument> CREATOR = new Parcelable.Creator<MyDocument>() {
        public MyDocument createFromParcel(Parcel in) {
        	try {
        		return new MyDocument(in);
        	}
        	catch (JSONException exception) {
        		return null;
        	}
        	catch (TagByException exception) {
        		return null;
        	}
        }

        public MyDocument[] newArray(int size) {
            return new MyDocument[size];
        }
    };
    
    /**
     * This is an example of a mandatory logo Image widget.
     */
    public TagByImage getLogo() {
		return super.getWidgets().getItem(WIDGET_LOGO);
	}
    
    /**
     * This is an example of a optional Image widget.
     */
    public TagByImage getImage() {
		return super.getWidgets().optItem(WIDGET_IMAGE);
	}
    
    /**
     * Another example of optional label. IF this item is missing
     * in the document it will be hidden by associated widget controller.
     */
    public TagByLabel getInfoLabel() {
		return super.getWidgets().optItem(WIDGET_INFO_LABEL);
	}
    
    /**
     * And this button widget must be always presents.
     */
    public TagByButton getShareButton() {
		return super.getWidgets().getItem(WIDGET_SHARE_BUTTON);
	}
}