package com.tagby.sdk.resources;

import android.content.Context;

public final class ResourceUtils {
	public static final String PACKAGE_NAME;
	public static final String RESOURCE_CLASS_NAME;
	
	static {
		PACKAGE_NAME = ResourceUtils.class.getPackage().getName();
		RESOURCE_CLASS_NAME = PACKAGE_NAME + ".R";
	}
	
	private ResourceUtils() {
	}
	
	// Strongly-typed resources
	
	public static boolean isLandscape(Context context) {
		return ResourceUtils.getBoolean(context, "isLandscape");
	}
	
	public static String tagby_manager_service_not_found(Context context) {
		return ResourceUtils.getString(context, "tagby_manager_service_not_found");
	}
	
	public static String initializing_device(Context context) {
		return ResourceUtils.getString(context, "initializing_device");
	}
	
	public static String retrieving_documents(Context context) {
		return ResourceUtils.getString(context, "retrieving_documents");
	}
	
	public static String missing_document_orientation(Context context) {
		return ResourceUtils.getString(context, "missing_document_orientation");
	}
	
	public static String unsupported_document_orientation(Context context) {
		return ResourceUtils.getString(context, "unsupported_document_orientation");
	}
	
	public static String duplicated_widget_name(Context context) {
		return ResourceUtils.getString(context, "duplicated_widget_name");
	}
	
	public static String missing_widget_name(Context context) {
		return ResourceUtils.getString(context, "missing_widget_name");
	}
	
	public static String no_documents_configured_for_orientation(Context context) {
		return ResourceUtils.getString(context, "no_documents_configured_for_orientation");
	}
	
	public static String camera_open_error(Context context) {
		return ResourceUtils.getString(context, "camera_open_error");
	}
	
	public static String checking_tag(Context context) {
		return ResourceUtils.getString(context, "checking_tag");
	}
	
	public static String unknown_user_registration_required(Context context) {
		return ResourceUtils.getString(context, "unknown_user_registration_required");
	}
	
	public static String registration_cancelled(Context context) {
		return ResourceUtils.getString(context, "registration_cancelled");
	}
	
	public static String publishing(Context context) {
		return ResourceUtils.getString(context, "publishing");
	}
	
	public static String already_scanned(Context context) {
		return ResourceUtils.getString(context, "already_scanned");
	}
	
	public static String crash_report_info(Context context) {
		return ResourceUtils.getString(context, "crash_report_info");
	}
	
	public static String crash_report_title(Context context) {
		return ResourceUtils.getString(context, "crash_report_title");
	}
	
	public static String crash_report_subject(Context context) {
		return ResourceUtils.getString(context, "crash_report_subject");
	}
	
	public static String documents_filtered_nfc(Context context) {
		return ResourceUtils.getString(context, "documents_filtered_nfc");
	}
	
	public static String no_network_connection(Context context) {
		return ResourceUtils.getString(context, "no_network_connection");
	}
	
	public static String cancel(Context context) {
		return ResourceUtils.getString(context, "cancel");
	}
	
	public static int activity_webview() {
		return ResourceUtils.getLayoutId("activity_webview");
	}
	
	public static int activity_webview_web_view() {
		return ResourceUtils.getId("activity_webview_web_view");
	}
	
	private static int getId(String name) {
		return getResourceIdByName("id", name);
	}
	
	private static boolean getBoolean(Context context, String name) {
		return context.getResources().getBoolean(
			getResourceIdByName("bool", name));
	}

	private static String getString(Context context, String name) {
		return context.getString(
			getResourceIdByName("string", name));
	}
	
	private static int getLayoutId(String name) {
		return getResourceIdByName("layout", name);
	}
	
	private static int getResourceIdByName(String className, String name) {
	    Class<?> r = null;
	    int id = 0;
	    try {
	        r = Class.forName(RESOURCE_CLASS_NAME);

	        Class<?>[] classes = r.getClasses();
	        Class<?> desireClass = null;

	        for (int i = 0; i < classes.length; i++) {
	            if (classes[i].getName().split("\\$")[1].equals(className)) {
	                desireClass = classes[i];

	                break;
	            }
	        }

	        if (desireClass != null) {
	            id = desireClass.getField(name).getInt(desireClass);
	        }

	    } catch (ClassNotFoundException e) {
	        e.printStackTrace();
	    } catch (IllegalArgumentException e) {
	        e.printStackTrace();
	    } catch (SecurityException e) {
	        e.printStackTrace();
	    } catch (IllegalAccessException e) {
	        e.printStackTrace();
	    } catch (NoSuchFieldException e) {
	        e.printStackTrace();
	    }

	    return id;
	}
}