package com.tagby.sdk.simpleapp.fragments;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.ErrorReason;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.PlayerStateChangeListener;
import com.google.android.youtube.player.YouTubePlayer.PlayerStyle;
import com.google.android.youtube.player.YouTubePlayer.Provider;

import com.tagby.sdk.app.MessagePresenter;
import com.tagby.sdk.app.TagByFragment;
import com.tagby.sdk.simpleapp.R;
import com.tagby.sdk.simpleapp.YouTubeListener;
import com.tagby.sdk.utils.Check;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.GetChars;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class YouTubePlayerFragment extends TagByFragment {
	private static final String ARGS_API_KEY = "apiKey";
	private static final String ARGS_VIDEO_ID = "videoId";
	
	private final YouTubeStateChangeListener mPlayerStateChangeListener = new YouTubeStateChangeListener();
	private com.google.android.youtube.player.YouTubePlayerFragment mYouTubePlayerFragment; 
	private YouTubePlayer mPlayer;
	private int mActivityRequestedOrientation;
	
	public static YouTubePlayerFragment newInstance(String apiKey, String videoId) {
		Check.Argument.isNotNull(apiKey, "apiKey");
		Check.Argument.isNotNull(videoId, "videoId");
		
		Bundle args = new Bundle();
		args.putString(ARGS_API_KEY, apiKey);
		args.putString(ARGS_VIDEO_ID, videoId);
		
		YouTubePlayerFragment f = new YouTubePlayerFragment();
		f.setArguments(args);
		
		return f;
	}
	
	public String getApiKey() {
		return getArguments().getString(ARGS_API_KEY);
	}
	
	public String getVideoId() {
		return getArguments().getString(ARGS_VIDEO_ID);
	}
	
	
	@Override
	protected View safelyOnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) throws Exception {
		return inflater.inflate(R.layout.fragment_youtube_player, container, false);
	}
	
	@Override
	protected void safelyOnStart() throws Exception {
		//mYouTubePlayerFragment = (com.google.android.youtube.player.YouTubePlayerFragment)getFragmentManager().findFragmentById(R.id.fragment_youtube_player_main);
		mYouTubePlayerFragment = com.google.android.youtube.player.YouTubePlayerFragment.newInstance();
		
		getFragmentManager().beginTransaction().replace(R.id.fragment_youtube_player_main, mYouTubePlayerFragment).commit();
	}
	
	@Override
	protected void safelyOnResume() throws Exception {
		mActivityRequestedOrientation = getActivity().getRequestedOrientation();
		getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		
		if (mPlayer == null) {			
			mYouTubePlayerFragment.initialize(getApiKey(), new OnInitializedListener() {

				@Override
				public void onInitializationFailure(Provider arg0, YouTubeInitializationResult arg1) {
					MessagePresenter.getInstance().showInformation(arg1.toString());
				}

				@Override
				public void onInitializationSuccess(Provider provider, YouTubePlayer player, boolean wasResumed) {
					try {
						mPlayer = player;
						mPlayer.setPlayerStyle(PlayerStyle.MINIMAL);
						mPlayer.setPlayerStateChangeListener(mPlayerStateChangeListener);
						mPlayer.cueVideo(getVideoId());
					}
					catch (Exception exception) {
						MessagePresenter.getInstance().showException(exception);
					}
				}
			});
		}
		else {
			mPlayer.cueVideo(getVideoId());
		}
	}
	
	@Override
	protected void safelyOnPause() throws Exception {
		getActivity().setRequestedOrientation(mActivityRequestedOrientation);
		
		if (mPlayer != null) {
			mPlayer.release();
			mPlayer = null;
		}
	}
	
	private class YouTubeStateChangeListener implements PlayerStateChangeListener {

		@Override
		public void onAdStarted() {
			Log.d(TAG, "onAdStarted");
		}

		@Override
		public void onError(ErrorReason reason) {
			Log.d(TAG, String.format("onError (%s)", reason));
		}

		@Override
		public void onLoaded(String videoId) {
			Log.d(TAG, String.format("onLoaded (%s)", videoId));
			mPlayer.play();
		}

		@Override
		public void onLoading() {
			Log.d(TAG, "onLoading");
		}

		@Override
		public void onVideoEnded() {
			Log.d(TAG, "onVideoEnded");
			
			if (getActivity() instanceof YouTubeListener) {
				((YouTubeListener)getActivity()).onPlayYouTubeVideoEnded(getVideoId());
			}
		}

		@Override
		public void onVideoStarted() {
			Log.d(TAG, "onVideoStarted");
		}
	}
}