package com.tagby.sdk.simpleapp;

import java.util.HashSet;
import java.util.Set;

import org.json.JSONException;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.tagby.sdk.TagByUser;
import com.tagby.sdk.api.EventCommand;
import com.tagby.sdk.app.MessagePresenter;
import com.tagby.sdk.app.TagByActivity;
import com.tagby.sdk.app.TagByApplication;
import com.tagby.sdk.app.TagByAsyncTask;
import com.tagby.sdk.app.TagByManualRegistrationActivity;
import com.tagby.sdk.documents.TagByDocument;
import com.tagby.sdk.simpleapp.fragments.SelectDocumentFragment;
import com.tagby.sdk.simpleapp.fragments.ShareAddFriendFragment;
import com.tagby.sdk.simpleapp.fragments.ShareAddFriendSocialTagFragment;
import com.tagby.sdk.simpleapp.fragments.ShareNoUserFragment;
import com.tagby.sdk.simpleapp.fragments.ShareSocialTagFragment;
import com.tagby.sdk.simpleapp.fragments.ShareUsersFragment;
import com.tagby.sdk.simpleapp.fragments.SplashScreenFragment;
import com.tagby.sdk.simpleapp.fragments.YouTubePlayerFragment;
import com.tagby.sdk.utils.Check;
import com.tagby.sdk.utils.MultipleImagesDownloader;
import com.tagby.sdk.utils.MultipleImagesDownloader.DownloadListener;

public class SimpleTagByActivity<T extends TagByDocument> extends TagByActivity<T> implements
	SelectDocumentFragment.DocumentSelectionListener<T>,
	ShareNoUserFragment.Listener,
	ShareUsersFragment.Listener,
	ShareAddFriendFragment.Listener,
	ShareAddFriendSocialTagFragment.Listener,
	ShareSocialTagFragment.Listener,
	DocumentSwitchListener,
	YouTubeListener,
	IProvideSendingEvents {
	
	private final static String TAG_SELECT_DOCUMENT = "selectDocument";
	private final static String TAG_SPLASH_SCREEN = "splashScreen";
	private final static String TAG_SHARE_NO_USER = "shareNoUser";
	private final static String TAG_SHARE_USERS = "shareUsers";
	private final static String TAG_SHARE_SOCIAL_TAG = "shareSocialTag";
	private final static String TAG_SHARE_ADD_FRIEND = "shareAddFriend";
	private final static String TAG_SHARE_ADD_FRIEND_SOCIAL_TAG = "shareAddFriendSocialTag";
	private final static String TAG_SHARE_YOUTUBE_PLAYER = "youTubePlayer";
	
	private T mDocument;
	private SplashScreenFragment mSplashScreenFragment;

	public T getDocument() {
		return mDocument;
	}
		
	@Override
	protected void safelyOnCreate(Bundle savedInstanceState) throws Exception {
		super.safelyOnCreate(savedInstanceState);
		
		setContentView(R.layout.activity_simple);
		
		final FragmentManager fm = getFragmentManager();
		
		mSplashScreenFragment = (SplashScreenFragment)fm.findFragmentByTag(TAG_SPLASH_SCREEN);
		if (mSplashScreenFragment == null) {
			mSplashScreenFragment = new SplashScreenFragment();
			fm.beginTransaction().add(
				R.id.main_frame,
				mSplashScreenFragment,
				TAG_SPLASH_SCREEN).commit();
		}
	}
	
	@Override
	protected void applyDocument(T ignored) throws Exception {
		navigateToSelectDocuments ();
		/*
		Set<String> previewUrls = new HashSet<String>();
		for (TagByDocument document: TagByApplication.getInstance().getDocuments().getDocuments()) {
			document.addImageUrls(previewUrls);
		}
		
		if (previewUrls.size() == 0) {
			navigateToSelectDocuments ();
			return;
		}
		
		MultipleImagesDownloader imageDownloader = new MultipleImagesDownloader(previewUrls);
		imageDownloader.downloadAll(
			new DownloadListener() {			
				@Override
				public void onDownloaded() {
					try {
						navigateToSelectDocuments ();
					}
					catch (Exception exception) {
						MessagePresenter.getInstance().showException(exception);
					}
				}
			});
		*/
	}
	
	private void navigateToSelectDocuments() throws Exception {
		if (mSplashScreenFragment != null) {
			mSplashScreenFragment = null;
			
			final FragmentManager fm = getFragmentManager();
			
			fm.beginTransaction()
			.replace(R.id.main_frame, new SelectDocumentFragment<T>(), TAG_SELECT_DOCUMENT)
			.commit();

			if (TagByApplication.getInstance().getDocuments().getDocuments().size() == 1) {
				onDocumentSelected((T)TagByApplication.getInstance().getDocuments().getDocuments().get(0));
			}
		}
	}
	
	@Override
	protected void onDocumentsRefreshed() throws Exception {
		super.onDocumentsRefreshed();
		
		SelectDocumentFragment<T> selectDocumentFragment = (SelectDocumentFragment<T>)getFragmentManager().findFragmentByTag(TAG_SELECT_DOCUMENT);
		
		if (selectDocumentFragment != null)
			selectDocumentFragment.onDocumentsChanged();
	}
	
	@Override
	public void onDocumentSelected(T document) throws Exception {
		mDocument = document;
	}
		
	public void onShare() throws JSONException {
		Log.d(TAG, "onShare");
	
		TagByApplication.getInstance().getUsers().removeAll();

		getFragmentManager()
		.beginTransaction()
		.add(R.id.main_frame, ShareNoUserFragment.newInstance(mDocument), TAG_SHARE_NO_USER)
		.addToBackStack(null)
		.commit();
	}
	
	@Override
	public void onShareClosed() throws Exception {
		Log.d(TAG, "onShareClosed");
		
		final FragmentManager fm = getFragmentManager();
		
		fm.popBackStackImmediate();
	}
	
	
	@Override
	public void onShareNow() throws Exception {
		Log.d(TAG, "onShareNow");
		
		//new PublishTask().execute();
	}
	
	@Override
	public void onPublished() {
		Log.d(TAG, "onPublished");
		
		try {
			super.onPublished();
			
			MessagePresenter.getInstance().showInformation(getString(R.string.fragment_share_facebook_success));		
			onShareClosed();
			//mPhoto = null;
		}
		catch (Exception exception) {
			MessagePresenter.getInstance().showException(exception);
		}
	}
	
	@Override
	public void onAddFriend() throws Exception {
		Log.d(TAG, "onAddFriend");
		
		FragmentManager fm = getFragmentManager();
						
		fm
		.beginTransaction()
		.remove(fm.findFragmentByTag(TAG_SHARE_USERS))
		.add(R.id.main_frame, ShareAddFriendFragment.newInstance(mDocument), TAG_SHARE_ADD_FRIEND)
		.addToBackStack(null)
		.commit();
	}
	
	@Override
	public void onAddFriendClosed() throws Exception {
		Log.d(TAG, "onAddFriendClosed");
		
		final FragmentManager fm = getFragmentManager();
		
		fm.popBackStackImmediate();
		fm.popBackStackImmediate();
		//((TakePhotoFragment)fm.findFragmentByTag(TAG_TAKE_PHOTO)).restart();
	}
	
	@Override
	public void onAddFriendCancelled() throws Exception {
		Log.d(TAG, "onAddFriendCancelled");
		
		getFragmentManager().popBackStackImmediate();
	}
	
	@Override
	protected void onUserAdded(TagByUser user) {
		Log.d(TAG, "onUserAdded");
		
		try {
			super.onUserAdded(user);
			
			sendEvent(
				EventNames.SHARE_USERS_SHOWN,
				user);
			
			FragmentManager fm = getFragmentManager();
			
			ShareAddFriendSocialTagFragment addFriendSocialTagFragment = (ShareAddFriendSocialTagFragment)fm.findFragmentByTag(TAG_SHARE_ADD_FRIEND_SOCIAL_TAG);
			if (addFriendSocialTagFragment != null) {
				fm.popBackStackImmediate();
				fm.popBackStackImmediate();
				return;
			}
			
			ShareAddFriendFragment addFriendFragment = (ShareAddFriendFragment)fm.findFragmentByTag(TAG_SHARE_ADD_FRIEND);
			if (addFriendFragment != null) {
				fm.popBackStackImmediate();
				return;
			}
			
			ShareUsersFragment usersFragment = (ShareUsersFragment)fm.findFragmentByTag(TAG_SHARE_USERS);
			if (usersFragment != null) {				
				usersFragment.addUser(user);				
				return;
			}
			
			ShareSocialTagFragment shareSocialTagFragment = (ShareSocialTagFragment)fm.findFragmentByTag(TAG_SHARE_SOCIAL_TAG);
			if (shareSocialTagFragment != null) {
				fm.popBackStackImmediate();
				fm.popBackStackImmediate();
				fm
				.beginTransaction()
				.add(R.id.main_frame, ShareUsersFragment.newInstance(mDocument), TAG_SHARE_USERS)
				.addToBackStack(null)
				.commit();
			}
			
			ShareNoUserFragment noUserFragment = (ShareNoUserFragment)fm.findFragmentByTag(TAG_SHARE_NO_USER);
			if (noUserFragment != null) {
				fm.popBackStackImmediate();
				fm
				.beginTransaction()
				.add(R.id.main_frame, ShareUsersFragment.newInstance(mDocument), TAG_SHARE_USERS)
				.addToBackStack(null)
				.commit();
			}
		}
		catch (Exception exception) {
			MessagePresenter.getInstance().showException(exception);
		}
	}
	
	@Override
	public void onSocialTag() throws Exception {
		Log.d(TAG, "onSocialTag");
		
		FragmentManager fm = getFragmentManager();
		
		fm
		.beginTransaction()
		.remove(fm.findFragmentByTag(TAG_SHARE_NO_USER))
		.add(R.id.main_frame, ShareSocialTagFragment.newInstance(mDocument), TAG_SHARE_SOCIAL_TAG)
		.addToBackStack(null)
		.commit();
	}
	
	@Override
	public void onAddFriendSocialTag() throws Exception {
		Log.d(TAG, "onAddFriendSocialTag");
		
		FragmentManager fm = getFragmentManager();
		
		getFragmentManager()
		.beginTransaction()
		.remove(fm.findFragmentByTag(TAG_SHARE_ADD_FRIEND))
		.add(R.id.main_frame, ShareAddFriendSocialTagFragment.newInstance(mDocument), TAG_SHARE_ADD_FRIEND_SOCIAL_TAG)
		.addToBackStack(null)
		.commit();
	}
	
	@Override
	public void onAddFriendSocialTagClosed() throws Exception {
		Log.d(TAG, "onAddFriendSocialTagClosed");
		
		final FragmentManager fm = getFragmentManager();
		
		fm.popBackStackImmediate();
		fm.popBackStackImmediate();
		
		onShareClosed();
	}
	
	@Override
	public void onAddFriendSocialTagCancelled() throws Exception {
		Log.d(TAG, "onAddFriendSocialTagCancelled");
		
		getFragmentManager().popBackStackImmediate();
	}
	
	@Override
	public void onSocialTagClosed() throws Exception {
		Log.d(TAG, "onAddSocialTagCancelled");
		
		final FragmentManager fm = getFragmentManager();

		fm.popBackStackImmediate();
		
		onShareClosed();
	}
	
	@Override
	public void onSocialTagCancelled() throws Exception {
		Log.d(TAG, "onAddSocialTagCancelled");
		
		getFragmentManager().popBackStackImmediate();
	}
	
	@Override
	public void onConnectFacebook() throws Exception {
		Log.d(TAG, "onConnectFacebook");
		
		connect(TagByManualRegistrationActivity.SOCIAL_NETWORK_FACEBOOK);
	}
	
	@Override
	public void onConnectTwitter() throws Exception {
		Log.d(TAG, "onConnectTwitter");
		
		connect(TagByManualRegistrationActivity.SOCIAL_NETWORK_TWITTER);
	}
	
	@Override
	public void onConnectEmail() throws Exception {
		Log.d(TAG, "onConnectEmail");
		
		connect(TagByManualRegistrationActivity.SOCIAL_NETWORK_EMAIL);
	}
	
	private void connect(String socialNetwork) throws Exception {
		Check.Argument.isNotNull(socialNetwork, "socialNetwork");
		
		Log.d(TAG, String.format("connect (%s)", socialNetwork));
		
		Intent intent = new Intent(this, TagByManualRegistrationActivity.class);
		intent.putExtra(TagByManualRegistrationActivity.EXTRA_DEVICE_ID, this.getRemoteDeviceId());
		intent.putExtra(TagByManualRegistrationActivity.EXTRA_OFFER_ID, this.getOfferId());
		intent.putExtra(TagByManualRegistrationActivity.EXTRA_SOCIAL_NETWORK, socialNetwork);
		
		//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		this.startActivityForResult(intent, TagByActivity.REQUEST_REGISTER_MANUAL);
	}
	
	@Override
	protected void safelyOnBackPressed() throws Exception {
		Log.d(TAG, "safelyOnBackPressed");
		
		FragmentManager fm = getFragmentManager();
		if (fm.getBackStackEntryCount() != 0) {
			if (fm.findFragmentByTag(TAG_SHARE_ADD_FRIEND_SOCIAL_TAG) != null) {
				onAddFriendSocialTagCancelled();
				return;
			}
			
			if (fm.findFragmentByTag(TAG_SHARE_ADD_FRIEND) != null) {
				onAddFriendCancelled();
				return;
			}
			
			if (fm.findFragmentByTag(TAG_SHARE_SOCIAL_TAG) != null) {
				onSocialTagCancelled();
				return;
			}
			
			if (fm.findFragmentByTag(TAG_SHARE_NO_USER) != null ||
				fm.findFragmentByTag(TAG_SHARE_USERS) != null) {
				onShareClosed();
				return;
			}
		}
		
		super.safelyOnBackPressed();
	}
	
	@Override
	public void onDocumentSwitch() throws Exception {
		Log.d(TAG, "onDocumentSwitch");
		
		getFragmentManager().popBackStackImmediate();
	}

	@Override
	public void onPlayYouTube(String apiKey, String videoId) {
		FragmentManager fm = getFragmentManager();
		
		fm
		.beginTransaction()
		.replace(R.id.main_frame, YouTubePlayerFragment.newInstance(apiKey, videoId), TAG_SHARE_YOUTUBE_PLAYER)
		.addToBackStack(null)
		.commit();
	}
	
	@Override
	public void onPlayYouTubeVideoEnded(String videoId) {
		Log.d(TAG, "onDocumentSwitch");
		
		getFragmentManager().popBackStackImmediate();
	}
	
	public void sendEvent(String eventName) {
		sendEvent(eventName, null);
	}
	
	public void sendEvent(String eventName, TagByUser user) {
		Check.Argument.isNotNull(eventName, "eventName");
		
		new SendAnalyticsEvent(eventName, user).execute();
	}
	
	private class SendAnalyticsEvent extends TagByAsyncTask<Void, Void, Void> {
		private final String mEventName;
		private final TagByUser mUser;
		
		protected SendAnalyticsEvent(
			String eventName,
			TagByUser user) {
			super(SimpleTagByActivity.this, null, true);
		
			Check.Argument.isNotNull(eventName, "eventName");
			
			mEventName = eventName;
			mUser = user;
		}

		@Override
		protected Void safelyDoInBackground(Void... params) throws Exception {
			new EventCommand(
				SimpleTagByActivity.this.getRemoteDeviceId(),
				SimpleTagByActivity.this.getOfferId(),
				mEventName,
				mUser).execute();
			
			return null;
		}		
		
		@Override
		protected void onSuccess(Void result) {
			Log.d(TAG, String.format("Send event %s succeeded.", mEventName));
		}
		
		@Override
		protected void onFailure(Exception exception) {
			Log.d(TAG, String.format("Send event %s failed: %s.", mEventName, exception.toString()));
		}
	}
}