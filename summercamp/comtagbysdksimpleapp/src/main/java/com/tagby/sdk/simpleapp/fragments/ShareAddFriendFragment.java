package com.tagby.sdk.simpleapp.fragments;

import com.tagby.sdk.app.TagByApplication;
import com.tagby.sdk.app.TagByDocumentFragment;
import com.tagby.sdk.app.TagByOnClickListener;
import com.tagby.sdk.documents.TagByDocument;
import com.tagby.sdk.simpleapp.EventNames;
import com.tagby.sdk.simpleapp.IProvideSendingEvents;
import com.tagby.sdk.simpleapp.R;
import com.tagby.sdk.utils.Check;
import com.tagby.sdk.utils.ViewUtils;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

public class ShareAddFriendFragment extends TagByDocumentFragment<TagByDocument> {
	public interface Listener {
		public void onAddFriendClosed() throws Exception;
		public void onAddFriendCancelled() throws Exception;
		public void onAddFriendSocialTag() throws Exception;
		public void onConnectFacebook() throws Exception;
		public void onConnectTwitter() throws Exception;
		public void onConnectEmail() throws Exception;
	}
	
	public static ShareAddFriendFragment newInstance (TagByDocument document) {
		Check.Argument.isNotNull(document, "document");
	
		ShareAddFriendFragment fragment = new ShareAddFriendFragment();

		fragment.setArguments(fragment.createBundle(document));
		
		return fragment;
	}
	
	@Override
	protected void safelyOnCreate(Bundle savedInstanceState) throws Exception {
		Log.d(TAG, "safelyOnCreate");
		
		super.safelyOnCreate(savedInstanceState);
		
		Activity activity = getActivity();
		
		if (activity instanceof IProvideSendingEvents) {
			((IProvideSendingEvents)activity).sendEvent(
					EventNames.SHARE_ADD_USER_SHOWN);
		}
	};
	
	@Override
	protected View safelyOnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) throws Exception {
		final TagByDocument document = getDocument();
		
		WindowManager wm = (WindowManager) TagByApplication.getInstance().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		
		float scalingX = metrics.widthPixels / 2560f;
		float scalingY = metrics.heightPixels / 1504f;
		
        View rootView = inflater.inflate(R.layout.fragment_share_add_friend, container, false);
        View close = rootView.findViewById(R.id.fragment_share_add_friend_close);
        View cancel = rootView.findViewById(R.id.fragment_share_add_friend_cancel);
        
        View socialTag = rootView.findViewById(R.id.fragment_share_add_friend_social_tag);
        View connectFacebook = rootView.findViewById(R.id.fragment_share_add_friend_facebook);
        View connectTwitter = rootView.findViewById(R.id.fragment_share_add_friend_twitter);
        View connectEmail = rootView.findViewById(R.id.fragment_share_add_friend_email);
        
        boolean facebookSupported = document.getSocialNetworks().getFacebook().isSupported();
        boolean twitterSuppported = document.getSocialNetworks().getTwitter().isSupported();
        boolean emailSuppported = document.getSocialNetworks().getEmail().isSupported();
        
        connectFacebook.setVisibility(facebookSupported ? View.VISIBLE : View.GONE);
        connectTwitter.setVisibility(twitterSuppported ? View.VISIBLE : View.GONE);
        connectEmail.setVisibility(emailSuppported ? View.VISIBLE : View.GONE);        
        	
        boolean isLandscape = TagByApplication.getInstance().isLandscape();
        
        if (isLandscape) {
	        if (facebookSupported == false) { 
	        	if (twitterSuppported == false) {
	        		connectEmail.setLayoutParams(connectFacebook.getLayoutParams());
	        	}
	        	else {
	        		connectEmail.setLayoutParams(connectTwitter.getLayoutParams());
	        		connectTwitter.setLayoutParams(connectFacebook.getLayoutParams());        		
	        	}
	        }
	        else if (twitterSuppported == false) {
	        	connectEmail.setLayoutParams(connectTwitter.getLayoutParams());
	        }
        }

        if (isLandscape) {
	        ViewUtils.scale(scalingX, scalingY, rootView.findViewById(R.id.fragment_share_add_friend_label));
	        ViewUtils.scale(scalingX, scalingY, close);
	        ViewUtils.scale(scalingX, scalingY, connectFacebook);
	        ViewUtils.scale(scalingX, scalingY, connectTwitter);
	        ViewUtils.scale(scalingX, scalingY, connectEmail);
	        ViewUtils.scale(scalingX, scalingY, cancel);
        }
       		
        close.setOnClickListener(
        		new TagByOnClickListener() {			
        			@Override
        			protected void safelyOnClick(View v) throws Exception {
        				((Listener)getActivity()).onAddFriendClosed();		
        			}
        		});
        
        cancel.setOnClickListener(
        		new TagByOnClickListener() {			
        			@Override
        			protected void safelyOnClick(View v) throws Exception {
        				((Listener)getActivity()).onAddFriendCancelled();		
        			}
        		});
        
        if (isLandscape == false) {
	        socialTag.setOnClickListener(
	            	new TagByOnClickListener() {				
	    				@Override
	    				protected void safelyOnClick(View v) throws Exception {
	    					((Listener)getActivity()).onAddFriendSocialTag();
	    				}
	    			});
        }
        
        connectFacebook.setOnClickListener(
        	new TagByOnClickListener() {				
				@Override
				protected void safelyOnClick(View v) throws Exception {
					((Listener)getActivity()).onConnectFacebook();
				}
			});
        
        connectTwitter.setOnClickListener(
            	new TagByOnClickListener() {				
    				@Override
    				protected void safelyOnClick(View v) throws Exception {
    					((Listener)getActivity()).onConnectTwitter();
    				}
    			});
        
        connectEmail.setOnClickListener(
            	new TagByOnClickListener() {				
    				@Override
    				protected void safelyOnClick(View v) throws Exception {
    					((Listener)getActivity()).onConnectEmail();
    				}
    			});
            
        return rootView;
	}
}