package com.tagby.sdk.simpleapp;

public interface DocumentSwitchListener {
	public void onDocumentSwitch() throws Exception;
}