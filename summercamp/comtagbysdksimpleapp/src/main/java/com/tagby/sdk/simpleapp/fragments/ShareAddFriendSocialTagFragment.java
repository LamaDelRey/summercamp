package com.tagby.sdk.simpleapp.fragments;

import org.json.JSONObject;

import com.tagby.sdk.app.TagByActivity;
import com.tagby.sdk.app.TagByDocumentFragment;
import com.tagby.sdk.app.TagByOnClickListener;
import com.tagby.sdk.app.controllers.TagByBarcodePreviewController;
import com.tagby.sdk.documents.TagByDocument;
import com.tagby.sdk.documents.widgets.TagByCameraSource;
import com.tagby.sdk.documents.widgets.TagByRect;
import com.tagby.sdk.documents.widgets.TagByWidgetType;
import com.tagby.sdk.documents.widgets.json.TagByBarcodePreview;
import com.tagby.sdk.documents.widgets.json.TagByCameraRelatedWidget;
import com.tagby.sdk.documents.widgets.json.TagByWidget;
import com.tagby.sdk.simpleapp.EventNames;
import com.tagby.sdk.simpleapp.IProvideSendingEvents;
import com.tagby.sdk.simpleapp.R;
import com.tagby.sdk.utils.Check;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class ShareAddFriendSocialTagFragment extends TagByDocumentFragment<TagByDocument> {
	private TagByBarcodePreviewController<TagByDocument> mBarcodePreviewController;
	private View mSwitchCamera;
	
	public interface Listener {
		public void onAddFriendSocialTagClosed() throws Exception;
		public void onAddFriendSocialTagCancelled() throws Exception;
	}
	
	public static ShareAddFriendSocialTagFragment newInstance (TagByDocument document) {
		Check.Argument.isNotNull(document, "document");
	
		ShareAddFriendSocialTagFragment fragment = new ShareAddFriendSocialTagFragment();

		fragment.setArguments(fragment.createBundle(document));
		
		return fragment;
	}
	
	@Override
	protected void safelyOnCreate(Bundle savedInstanceState) throws Exception {
		Log.d(TAG, "safelyOnCreate");
		
		super.safelyOnCreate(savedInstanceState);
		
		Activity activity = getActivity();
		
		if (activity instanceof IProvideSendingEvents) {
			((IProvideSendingEvents)activity).sendEvent(
					EventNames.SHARE_ADD_USER_SOCIAL_TAG_SHOWN);
		}
	};
	
	@Override
	protected View safelyOnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) throws Exception {
		final TagByDocument document = getDocument();
				
        View rootView = inflater.inflate(R.layout.fragment_share_add_friend_social_tag, container, false);
        View close = rootView.findViewById(R.id.fragment_share_add_friend_social_tag_close);
        ImageView photoZone = (ImageView)rootView.findViewById(R.id.fragment_share_add_friend_social_tag_photo_zone);
        FrameLayout cameraView = (FrameLayout)rootView.findViewById(R.id.fragment_share_add_friend_social_tag_camera_view);
        mSwitchCamera = rootView.findViewById(R.id.fragment_share_add_friend_social_tag_switch_camera);
        View cancel = rootView.findViewById(R.id.fragment_share_add_friend_social_tag_cancel);
                
        if (document.getSettings().nfcEnabled()) {
        	photoZone.setVisibility(View.GONE);
        	
        	cameraView.setVisibility(View.GONE);
        	mSwitchCamera.setVisibility(View.GONE);
        }
        else {
        	JSONObject json = new JSONObject();
        	json.put(TagByWidget.DATA_TYPE, TagByWidgetType.BARCODE_PREVIEW.getType());
        	json.put(TagByCameraRelatedWidget.DATA_SOURCE, TagByCameraSource.BACK_FRONT.getCode());
        	        		
        	json.put(TagByRect.DATA_X, 0);
        	json.put(TagByRect.DATA_Y, 0);
        	json.put(TagByRect.DATA_WIDTH, 0);
        	json.put(TagByRect.DATA_HEIGHT, 0);
        	
        	mBarcodePreviewController = new TagByBarcodePreviewController<TagByDocument>(
            		(TagByActivity<TagByDocument>)getActivity(),
            		cameraView,
            		new TagByBarcodePreview(json)) {
        		@Override
        		protected void applyPosition(com.tagby.sdk.documents.widgets.ITagByRect position) {
        			// does nothing by design
        		};
        	};

        	rootView.findViewById(R.id.fragment_share_add_friend_social_tag_rfid).setVisibility(View.GONE);
        	
        	cameraView.setVisibility(View.VISIBLE);        	        	
        }

        close.setOnClickListener(
        	new TagByOnClickListener() {			
				@Override
				protected void safelyOnClick(View v) throws Exception {
					((Listener)getActivity()).onAddFriendSocialTagClosed();		
				}
			});
        
        cancel.setOnClickListener(
            	new TagByOnClickListener() {			
    				@Override
    				protected void safelyOnClick(View v) throws Exception {
    					((Listener)getActivity()).onAddFriendSocialTagCancelled();		
    				}
    			});
                
        mSwitchCamera.setOnClickListener(new TagByOnClickListener() {			
			@Override
			protected void safelyOnClick(View v) throws Exception {
				if (mBarcodePreviewController != null && mBarcodePreviewController.canToogleCameraSource()) {
					mBarcodePreviewController.toogleCameraSource();
				}
			}
		});
        
        return rootView;
	}
	
	@Override
	protected void safelyOnResume() throws Exception {
		Log.d(TAG, "safelyOnResume");
		
		super.safelyOnResume();
		
		if (getDocument().getSettings().nfcEnabled()) {
			((TagByActivity<TagByDocument>)getActivity()).startNfc();	
		}
		else if (mBarcodePreviewController != null){
			mBarcodePreviewController.apply();
			
			mSwitchCamera.setVisibility(mBarcodePreviewController.canToogleCameraSource() ? View.VISIBLE : View.GONE);
		}
	}
	
	@Override
	protected void safelyOnPause() throws Exception {
		Log.d(TAG, "safelyOnPause");
		
		super.safelyOnPause();
		
		if (getDocument().getSettings().nfcEnabled()) {
			((TagByActivity<TagByDocument>)getActivity()).stopNfc();
		}
		else {
			mBarcodePreviewController.pause();
		}
	}
}