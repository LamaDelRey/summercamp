package com.tagby.sdk.simpleapp.fragments;

import org.json.JSONObject;

import com.tagby.sdk.app.TagByActivity;
import com.tagby.sdk.app.TagByApplication;
import com.tagby.sdk.app.TagByDocumentFragment;
import com.tagby.sdk.app.TagByOnClickListener;
import com.tagby.sdk.app.controllers.TagByBarcodePreviewController;
import com.tagby.sdk.documents.TagByDocument;
import com.tagby.sdk.documents.widgets.TagByCameraSource;
import com.tagby.sdk.documents.widgets.TagByRect;
import com.tagby.sdk.documents.widgets.TagByWidgetType;
import com.tagby.sdk.documents.widgets.json.TagByBarcodePreview;
import com.tagby.sdk.documents.widgets.json.TagByCameraRelatedWidget;
import com.tagby.sdk.documents.widgets.json.TagByWidget;
import com.tagby.sdk.simpleapp.EventNames;
import com.tagby.sdk.simpleapp.IProvideSendingEvents;
import com.tagby.sdk.simpleapp.R;
import com.tagby.sdk.utils.Check;
import com.tagby.sdk.utils.ViewUtils;

import android.app.Activity;
import android.content.Context;
import android.drm.DrmStore.Action;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class ShareNoUserFragment extends TagByDocumentFragment<TagByDocument> {
	private TagByBarcodePreviewController<TagByDocument> mBarcodePreviewController;
	private View mSwitchCamera;
	
	public interface Listener {
		public void onShareClosed() throws Exception;
		public void onSocialTag() throws Exception;
		public void onConnectFacebook() throws Exception;
		public void onConnectTwitter() throws Exception;
		public void onConnectEmail() throws Exception;
	}
	
	public static ShareNoUserFragment newInstance (TagByDocument document) {
		Check.Argument.isNotNull(document, "document");
	
		ShareNoUserFragment fragment = new ShareNoUserFragment();

		fragment.setArguments(fragment.createBundle(document));
		
		return fragment;
	}
	
	@Override
	protected void safelyOnCreate(Bundle savedInstanceState) throws Exception {
		Log.d(TAG, "safelyOnCreate");
		
		super.safelyOnCreate(savedInstanceState);
		
		Activity activity = getActivity();
		
		if (activity instanceof IProvideSendingEvents) {
			((IProvideSendingEvents)activity).sendEvent(
					EventNames.SHARE_NO_USER_SHOWN);
		}
	};	
	
	@Override
	protected View safelyOnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) throws Exception {
		final TagByDocument document = getDocument();
		
		WindowManager wm = (WindowManager) TagByApplication.getInstance().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		
		float scalingX = metrics.widthPixels / 2560f;
		float scalingY = metrics.heightPixels / 1504f;
		
        View rootView = inflater.inflate(R.layout.fragment_share_no_user, container, false);
        View close = rootView.findViewById(R.id.fragment_share_no_user_close);
        ImageView qrCode = (ImageView)rootView.findViewById(R.id.fragment_share_no_user_qr_code);
        View socialTagLabel = rootView.findViewById(R.id.fragment_share_no_user_social_tag_label);
        ImageView photoZone = (ImageView)rootView.findViewById(R.id.fragment_share_no_user_photo_zone);
        FrameLayout cameraView = (FrameLayout)rootView.findViewById(R.id.fragment_share_no_user_camera_view);
        mSwitchCamera = rootView.findViewById(R.id.fragment_share_no_user_switch_camera);
        
        View connectFacebook = rootView.findViewById(R.id.fragment_share_no_user_facebook);
        View connectTwitter = rootView.findViewById(R.id.fragment_share_no_user_twitter);
        View connectEmail = rootView.findViewById(R.id.fragment_share_no_user_email);
        View socialTag = rootView.findViewById(R.id.fragment_share_no_user_social_tag);
        
        boolean isLandscape = TagByApplication.getInstance().isLandscape();
        
        if (document.getSettings().nfcEnabled()) {
        	if (isLandscape) {
        		qrCode.setVisibility(View.GONE);
        	
	        	FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)socialTagLabel.getLayoutParams();        
	        	params.leftMargin = 567;
	        	socialTagLabel.setLayoutParams(params);
	        	
	        	params = (FrameLayout.LayoutParams)photoZone.getLayoutParams();
	        	params.topMargin = 639;
	        	params.height = 453;
	        	params.width = 390;
	        	photoZone.setLayoutParams(params);
	        	photoZone.setImageResource(R.drawable.fragment_share_photo_zone_rfid);
        	}
        	else {
        		photoZone.setVisibility(View.GONE);
        	}
        	
        	cameraView.setVisibility(View.GONE);
        	mSwitchCamera.setVisibility(View.GONE);
        }
        else {
        	JSONObject json = new JSONObject();
        	json.put(TagByWidget.DATA_TYPE, TagByWidgetType.BARCODE_PREVIEW.getType());
        	json.put(TagByCameraRelatedWidget.DATA_SOURCE, (isLandscape ? TagByCameraSource.FRONT_BACK : TagByCameraSource.BACK_FRONT).getCode());
        	
        	if (isLandscape) {
	        	FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)cameraView.getLayoutParams();
	
	        	json.put(TagByRect.DATA_X, (int)(scalingX * params.leftMargin));
	        	json.put(TagByRect.DATA_Y, (int)(scalingY * params.topMargin));
	        	json.put(TagByRect.DATA_WIDTH, (int)(scalingY * params.width));	        
	        	json.put(TagByRect.DATA_HEIGHT, (int)(scalingY * params.height));
	        	
	        	mBarcodePreviewController = new TagByBarcodePreviewController<TagByDocument>(
	            		(TagByActivity<TagByDocument>)getActivity(),
	            		cameraView,
	            		new TagByBarcodePreview(json));
        	}
        	else {        		
	        	json.put(TagByRect.DATA_X, 0);
	        	json.put(TagByRect.DATA_Y, 0);
	        	json.put(TagByRect.DATA_WIDTH, 0);
	        	json.put(TagByRect.DATA_HEIGHT, 0);
	        	
	        	mBarcodePreviewController = new TagByBarcodePreviewController<TagByDocument>(
	            		(TagByActivity<TagByDocument>)getActivity(),
	            		cameraView,
	            		new TagByBarcodePreview(json)) {
	        		@Override
	        		protected void applyPosition(com.tagby.sdk.documents.widgets.ITagByRect position) {
	        			// does nothing by design
	        		};
	        	};
        	}
        	
        	if (isLandscape == false) {
        		rootView.findViewById(R.id.fragment_share_no_user_rfid).setVisibility(View.GONE);
        	}
        	
        	cameraView.setVisibility(View.VISIBLE);        	        	
        }

        if (isLandscape) {
	        boolean facebookSupported = document.getSocialNetworks().getFacebook().isSupported();
	        boolean twitterSuppported = document.getSocialNetworks().getTwitter().isSupported();
	        boolean emailSuppported = document.getSocialNetworks().getEmail().isSupported();
	        
	        connectFacebook.setVisibility(facebookSupported ? View.VISIBLE : View.GONE);
	        connectTwitter.setVisibility(twitterSuppported ? View.VISIBLE : View.GONE);
	        connectEmail.setVisibility(emailSuppported ? View.VISIBLE : View.GONE);        
	        	
	        if (facebookSupported == false) { 
	        	if (twitterSuppported == false) {
	        		connectEmail.setLayoutParams(connectFacebook.getLayoutParams());
	        	}
	        	else {
	        		connectEmail.setLayoutParams(connectTwitter.getLayoutParams());
	        		connectTwitter.setLayoutParams(connectFacebook.getLayoutParams());        		
	        	}
	        }
	        else if (twitterSuppported == false) {
	        	connectEmail.setLayoutParams(connectTwitter.getLayoutParams());
	        }
        }
        
        if (isLandscape) {
	        ViewUtils.scale(scalingX, scalingY, rootView.findViewById(R.id.fragment_share_no_user_share_label));
	        ViewUtils.scale(scalingX, scalingY, close);
	        ViewUtils.scale(scalingX, scalingY, qrCode);
	        ViewUtils.scale(scalingX, scalingY, photoZone);
	        ViewUtils.scale(scalingX, scalingY, connectFacebook);
	        ViewUtils.scale(scalingX, scalingY, connectTwitter);
	        ViewUtils.scale(scalingX, scalingY, connectEmail);
	        ViewUtils.scale(scalingX, scalingY, socialTagLabel);
	        ViewUtils.scale(scalingX, scalingY, mSwitchCamera);
	        ViewUtils.scale(scalingX, scalingY, rootView.findViewById(R.id.fragment_share_no_user_register_label));
        }
        
        close.setOnClickListener(
        	new TagByOnClickListener() {			
				@Override
				protected void safelyOnClick(View v) throws Exception {
					((Listener)getActivity()).onShareClosed();		
				}
			});
        
        if (isLandscape) {
	        connectFacebook.setOnClickListener(
	        	new TagByOnClickListener() {				
					@Override
					protected void safelyOnClick(View v) throws Exception {
						((Listener)getActivity()).onConnectFacebook();
					}
				});
	        
	        connectTwitter.setOnClickListener(
	            	new TagByOnClickListener() {				
	    				@Override
	    				protected void safelyOnClick(View v) throws Exception {
	    					((Listener)getActivity()).onConnectTwitter();
	    				}
	    			});
	        
	        connectEmail.setOnClickListener(
	            	new TagByOnClickListener() {				
	    				@Override
	    				protected void safelyOnClick(View v) throws Exception {
	    					((Listener)getActivity()).onConnectEmail();
	    				}
	    			});
        }
        else {
        	socialTag.setOnClickListener(
	            	new TagByOnClickListener() {				
	    				@Override
	    				protected void safelyOnClick(View v) throws Exception {
	    					((Listener)getActivity()).onSocialTag();
	    				}
	    			});
        }
        
        mSwitchCamera.setOnClickListener(new TagByOnClickListener() {			
			@Override
			protected void safelyOnClick(View v) throws Exception {
				if (mBarcodePreviewController != null && mBarcodePreviewController.canToogleCameraSource()) {
					mBarcodePreviewController.toogleCameraSource();
				}
			}
		});
        
        return rootView;
	}
	
	@Override
	protected void safelyOnResume() throws Exception {
		Log.d(TAG, "safelyOnResume");
		
		super.safelyOnResume();
		
		if (getDocument().getSettings().nfcEnabled()) {
			((TagByActivity<TagByDocument>)getActivity()).startNfc();	
		}
		else if (mBarcodePreviewController != null){
			mBarcodePreviewController.apply();
			
			mSwitchCamera.setVisibility(mBarcodePreviewController.canToogleCameraSource() ? View.VISIBLE : View.GONE);
		}
	}
	
	@Override
	protected void safelyOnPause() throws Exception {
		Log.d(TAG, "safelyOnPause");
		
		super.safelyOnPause();
		
		if (getDocument().getSettings().nfcEnabled()) {
			((TagByActivity<TagByDocument>)getActivity()).stopNfc();
		}
		else {
			mBarcodePreviewController.pause();
		}
	}
}