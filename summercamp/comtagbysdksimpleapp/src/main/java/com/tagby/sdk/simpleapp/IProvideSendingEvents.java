package com.tagby.sdk.simpleapp;

import com.tagby.sdk.TagByUser;

public interface IProvideSendingEvents {
	public void sendEvent(String eventName);
	public void sendEvent(String eventName, TagByUser user);
}