package com.tagby.sdk.simpleapp;

public final class EventNames {
	private EventNames() {		
	}
	
	public static final String SHARE_NO_USER_SHOWN = "share_no_user_shown";
	public static final String SHARE_USERS_SHOWN = "share_users_shown";
	public static final String SHARE_ADD_USER_SHOWN = "share_add_user_shown";
	public static final String SHARE_NO_USER_NO_SOCIAL_TAG_SHOWN = "share_no_user_no_social_tag_shown";
	public static final String SHARE_ADD_USER_SOCIAL_TAG_SHOWN = "share_add_user_social_tag_shown";
}