package com.tagby.sdk.simpleapp;

public interface YouTubeListener {
	public void onPlayYouTube(String apiKey, String videoId);
	public void onPlayYouTubeVideoEnded(String videoId);
}