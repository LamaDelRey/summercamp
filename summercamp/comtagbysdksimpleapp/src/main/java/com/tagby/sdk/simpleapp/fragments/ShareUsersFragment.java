package com.tagby.sdk.simpleapp.fragments;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.toolbox.NetworkImageView;
import com.tagby.sdk.TagByUser;
import com.tagby.sdk.app.MessagePresenter;
import com.tagby.sdk.app.TagByActivity;
import com.tagby.sdk.app.TagByApplication;
import com.tagby.sdk.app.TagByDocumentFragment;
import com.tagby.sdk.app.TagByOnClickListener;
import com.tagby.sdk.app.controllers.TagByBarcodePreviewController;
import com.tagby.sdk.documents.TagByDocument;
import com.tagby.sdk.documents.widgets.TagByCameraSource;
import com.tagby.sdk.documents.widgets.TagByRect;
import com.tagby.sdk.documents.widgets.TagByWidgetType;
import com.tagby.sdk.documents.widgets.json.TagByBarcodePreview;
import com.tagby.sdk.documents.widgets.json.TagByCameraRelatedWidget;
import com.tagby.sdk.documents.widgets.json.TagByWidget;
import com.tagby.sdk.simpleapp.R;
import com.tagby.sdk.ui.NetworkImageProgressView;
import com.tagby.sdk.utils.Check;
import com.tagby.sdk.utils.ViewUtils;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class ShareUsersFragment extends TagByDocumentFragment<TagByDocument> {
	private LinearLayout mLandscapeUsersContainer = null;
	private PortraitUsersAdapter mPortraitUsersAdapter = null;
	private TagByBarcodePreviewController<TagByDocument> mBarcodePreviewController;
	private View mSwitchCamera;
	
	public interface Listener {
		public void onShareClosed() throws Exception;
		public void onShareNow() throws Exception;
		public void onAddFriend() throws Exception;
	}
	
	public static ShareUsersFragment newInstance (TagByDocument document) {
		Check.Argument.isNotNull(document, "document");
	
		ShareUsersFragment fragment = new ShareUsersFragment();

		fragment.setArguments(fragment.createBundle(document));
		
		return fragment;
	}
	
	@Override
	protected View safelyOnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) throws Exception {
		final TagByDocument document = getDocument();
		
		WindowManager wm = (WindowManager) TagByApplication.getInstance().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		
		float scalingX = metrics.widthPixels / 2560f;
		float scalingY = metrics.heightPixels / 1504f;
		
        View rootView = inflater.inflate(R.layout.fragment_share_users, container, false);        
        View close = rootView.findViewById(R.id.fragment_share_users_close);
        View shareNow = rootView.findViewById(R.id.fragment_share_users_share_now);
        View addFriend = rootView.findViewById(R.id.fragment_share_users_add_friend);
        ImageView photoZone = (ImageView)rootView.findViewById(R.id.fragment_share_users_photo_zone);
        FrameLayout cameraView = (FrameLayout)rootView.findViewById(R.id.fragment_share_users_camera_view);
        mSwitchCamera = rootView.findViewById(R.id.fragment_share_users_switch_camera);
        
        boolean isLandscape = TagByApplication.getInstance().isLandscape();
        
        if (isLandscape) {
	        if (document.getSettings().nfcEnabled()) {        	
	        	FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)photoZone.getLayoutParams();
	        	params.topMargin = 1005 - 29;
	        	params.height = 453;
	        	params.width = 390;
	        	photoZone.setLayoutParams(params);
	        	photoZone.setImageResource(R.drawable.fragment_share_photo_zone_rfid);
	        	
	        	cameraView.setVisibility(View.GONE);
	        	mSwitchCamera.setVisibility(View.GONE);
	        }
	        else {
	        	FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)cameraView.getLayoutParams();
	        	JSONObject json = new JSONObject();
	        	json.put(TagByWidget.DATA_TYPE, TagByWidgetType.BARCODE_PREVIEW.getType());
	        	json.put(TagByRect.DATA_X, (int)(scalingX * params.leftMargin));
	        	json.put(TagByRect.DATA_Y, (int)(scalingY * params.topMargin));
	        	json.put(TagByRect.DATA_WIDTH, (int)(scalingY * params.width));
	        	json.put(TagByRect.DATA_HEIGHT, (int)(scalingY * params.height));
	        	json.put(TagByCameraRelatedWidget.DATA_SOURCE, TagByCameraSource.FRONT_BACK.getCode());
	        	
	        	TagByBarcodePreview previewWidget = new TagByBarcodePreview(json);
	        	
	        	mBarcodePreviewController = new TagByBarcodePreviewController<TagByDocument>(
	        		(TagByActivity<TagByDocument>)getActivity(),
	        		cameraView,
	        		previewWidget);
	        	cameraView.setVisibility(View.VISIBLE);
	        }
        }
        
        if (isLandscape) {
        	mLandscapeUsersContainer = (LinearLayout)rootView.findViewById(R.id.fragment_share_users_users_container);
        	
	        ViewUtils.scale(scalingX, scalingY, rootView.findViewById(R.id.fragment_share_users_share_label));
	        ViewUtils.scale(scalingX, scalingY, close);
	        ViewUtils.scale(scalingX, scalingY, shareNow);
	        ViewUtils.scale(scalingX, scalingY, addFriend);
	        ViewUtils.scale(scalingX, scalingY, mLandscapeUsersContainer);
	        ViewUtils.scale(scalingX, scalingY, photoZone);
	        ViewUtils.scale(scalingX, scalingY, mSwitchCamera);
	        ViewUtils.scale(scalingX, scalingY, rootView.findViewById(R.id.fragment_share_users_add_friend_label));
        }
        else {
        	GridView portraitUsersContainer = (GridView)rootView.findViewById(R.id.fragment_share_users_users_container);
        	mPortraitUsersAdapter = new PortraitUsersAdapter(getActivity());
        	portraitUsersContainer.setAdapter(mPortraitUsersAdapter);
        }
        
        close.setOnClickListener(
        	new TagByOnClickListener() {			
			@Override
			protected void safelyOnClick(View v) throws Exception {
				((Listener)getActivity()).onShareClosed();				
			}
		});
        
        shareNow.setOnClickListener(
            	new TagByOnClickListener() {			
    			@Override
    			protected void safelyOnClick(View v) throws Exception {
    				((Listener)getActivity()).onShareNow();				
    			}
    		});
        
        if (isLandscape) {
	        addFriend.setOnClickListener(
	            	new TagByOnClickListener() {			
	    			@Override
	    			protected void safelyOnClick(View v) throws Exception {
	    				((Listener)getActivity()).onAddFriend();
	    			}
	    		});
	        
	        mSwitchCamera.setOnClickListener(new TagByOnClickListener() {			
				@Override
				protected void safelyOnClick(View v) throws Exception {
					if (mBarcodePreviewController != null && mBarcodePreviewController.canToogleCameraSource()) {
						mBarcodePreviewController.toogleCameraSource();
					}
				}
			});
        }
        
        return rootView;
	}

	public void addUser(TagByUser user) throws JSONException {
		Check.Argument.isNotNull(user, "user");
		
		if (mLandscapeUsersContainer != null) {
			addLandscapeUser(user);
		}
		else {
			addPortraitUser(user);
		}
	}
	
	private void addLandscapeUser(TagByUser user) throws JSONException {
		Check.Argument.isNotNull(user, "user");
		
		LinearLayout.LayoutParams panelParams = new LinearLayout.LayoutParams(170, 170);
		panelParams.leftMargin = mLandscapeUsersContainer.getChildCount() == 0 ? 0 : 50;

		RelativeLayout userPanel = new RelativeLayout(getActivity());
		userPanel.setLayoutParams(panelParams);
		
		RelativeLayout.LayoutParams imageParams = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		NetworkImageView imageView = new NetworkImageView(getActivity());
		imageView.setLayoutParams(imageParams);
		imageView.setScaleType(ScaleType.FIT_XY);
		imageView.setImageUrl(user.getFacebook().getPictureUrl(), TagByApplication.getInstance().getImageLoader());
		userPanel.addView(imageView);
		
		if (user.getFacebook() != null) {
			RelativeLayout.LayoutParams socialParams = new RelativeLayout.LayoutParams(24, 24);			
			socialParams.rightMargin = 10;
			socialParams.topMargin = 10;			
			socialParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
			socialParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
			ImageView socialView = new ImageView(getActivity());
			socialView.setScaleType(ScaleType.FIT_XY);
			socialView.setLayoutParams(socialParams);
			socialView.setImageResource(R.drawable.fragment_share_icn_facebook_xs);
			userPanel.addView(socialView);
		}
		
		mLandscapeUsersContainer.addView(userPanel);
	}
	
	private void addPortraitUser(TagByUser user) throws JSONException {
		Check.Argument.isNotNull(user, "user");
		
		mPortraitUsersAdapter.addUser(user);
	}
	
	@Override
	protected void safelyOnResume() throws Exception {
		super.safelyOnResume();
		
		if (mLandscapeUsersContainer != null) {
			mLandscapeUsersContainer.removeAllViews();
		
			for(TagByUser user: TagByApplication.getInstance().getUsers().getUsers()) {
				addUser(user);
			}
		}
		else {
			mPortraitUsersAdapter.setUsers(
					TagByApplication.getInstance().getUsers().getUsers());
		}
		
		if (TagByApplication.getInstance().isLandscape()) {
			if (getDocument().getSettings().nfcEnabled()) {
				((TagByActivity<TagByDocument>)getActivity()).startNfc();	
			}
			else if (mBarcodePreviewController != null){
				mBarcodePreviewController.apply();
				
				mSwitchCamera.setVisibility(mBarcodePreviewController.canToogleCameraSource() ? View.VISIBLE : View.GONE);
			}
		}
	}
	
	@Override
	protected void safelyOnPause() throws Exception {
		super.safelyOnPause();
		
		if (TagByApplication.getInstance().isLandscape()) {
			if (getDocument().getSettings().nfcEnabled()) {
				((TagByActivity<TagByDocument>)getActivity()).stopNfc();
			}
			else {
				mBarcodePreviewController.pause();
			}
		}
	}
	
	private class PortraitUsersAdapter extends BaseAdapter {
		private static final int TYPE_USER = 0;
		private static final int TYPE_TWO_USERS = 1;
		private static final int TYPE_ADD_USER = 2;
		private final Object mAddUserItem = new Object();
		
		private final LayoutInflater mInflater;
		private final List<TagByUser> mUsers;
		
		public PortraitUsersAdapter(Context context) {
			Check.Argument.isNotNull(context, "context");
			
			mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			mUsers = new ArrayList<TagByUser>();
		}
		
		public void addUser(TagByUser user) {
			Check.Argument.isNotNull(user, "user");
			
			mUsers.add(user);
			notifyDataSetChanged();
		}
		
		public void setUsers(Collection<TagByUser> users) {
			mUsers.clear();
			mUsers.addAll(users);
			notifyDataSetChanged();
		}
		
		@Override
		public int getCount() {
			return (mUsers.size() + 2) / 2;
		}

		@Override
		public Object getItem(int position) {
			if (2 * position >= mUsers.size()) {
				return mAddUserItem;
			}
			
			if (2 * position  + 1 < mUsers.size()) {
				return new TwoUsers(
					mUsers.get(2 * position),
					mUsers.get(2 * position + 1));
			}
			
			return mUsers.get(2 * position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public int getItemViewType(int position) {
			Object item = getItem(position);
			
			if (item == mAddUserItem)
				return TYPE_ADD_USER;
			else if (item instanceof TwoUsers)
				return TYPE_TWO_USERS;
			else
				return TYPE_USER; 
	    }

		@Override
	    public int getViewTypeCount() {
	        return 3;
	    }
	    
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Object item = getItem(position);
			
			if (mAddUserItem == item) {
				return getAddUserView(convertView, parent);
			}
			else if (item instanceof TwoUsers) {
				TwoUsers twoUsers = (TwoUsers)item;
				
				return getTwoUsersView(twoUsers.getUser1(), twoUsers.getUser2(), convertView, parent);
			}
			else {
				return getSingleUserView((TagByUser)item, convertView, parent);
			}
		}
		
		private View getAddUserView(View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.fragment_share_add_friend_button, parent, false);				
				setupAddFriendView(convertView.findViewById(R.id.fragment_share_add_friend_button));
			}			
			
			return convertView;
		}
		
		private View getSingleUserView(TagByUser user, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.fragment_share_facebook_user_add_friend_button, parent, false);
				setupAddFriendView(convertView.findViewById(R.id.fragment_share_add_friend_button));
			}
			
			setupUserView(user, (NetworkImageProgressView)convertView.findViewById(R.id.fragment_share_facebook_user_image));
			
			return convertView;
		}
		
		private View getTwoUsersView(TagByUser user1, TagByUser user2, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.fragment_share_facebook_user_facebook_user, parent, false);
			}
			
			setupUserView(user1, (NetworkImageProgressView)convertView.findViewById(R.id.fragment_share_facebook_user_image1));
			setupUserView(user2, (NetworkImageProgressView)convertView.findViewById(R.id.fragment_share_facebook_user_image2));
						
			return convertView;
		}
		
		private void setupAddFriendView(View addFriendView) {
			Check.Argument.isNotNull(addFriendView, "addFriendView");
			
			addFriendView.setOnClickListener(
				new TagByOnClickListener() {						
					@Override
					protected void safelyOnClick(View v) throws Exception {
						((Listener)getActivity()).onAddFriend();
					}
				});
		}
		
		private void setupUserView(TagByUser user, NetworkImageProgressView imageView) {
			Check.Argument.isNotNull(user, "user");
			Check.Argument.isNotNull(imageView, "imageView");
			
			try {
				imageView.setCornerRadius(10);
				imageView.setImageUrl(
						user.getFacebook().getPictureUrl(),
					TagByApplication.getInstance().getImageLoader());
			}
			catch (JSONException exception) {
				MessagePresenter.getInstance().showException(exception);
			}
		}
		
		private class TwoUsers {
			private final TagByUser mUser1;
			private final TagByUser mUser2;
			
			public TwoUsers(TagByUser user1, TagByUser user2) {
				mUser1 = user1;
				mUser2 = user2;
			}
			
			public TagByUser getUser1() {
				return mUser1;
			}
			
			public TagByUser getUser2() {
				return mUser2;
			}
		}
	}
}