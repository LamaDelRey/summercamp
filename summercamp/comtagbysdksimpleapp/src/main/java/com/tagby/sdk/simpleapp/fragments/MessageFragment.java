package com.tagby.sdk.simpleapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.tagby.sdk.app.TagByApplication;
import com.tagby.sdk.app.TagByFragment;
import com.tagby.sdk.simpleapp.R;
import com.tagby.sdk.utils.Check;
import com.tagby.sdk.utils.ViewUtils;

public class MessageFragment extends TagByFragment {
	private static final String ARGS_MESSAGE = "message";
	
	private TextView mMessageView = null;
	
	public static MessageFragment newInstance(String message) {
		Check.Argument.isNotNull(message, "message");
		
		Bundle args = new Bundle();
		args.putString(ARGS_MESSAGE, message);
		
		MessageFragment f = new MessageFragment();
		f.setArguments(args);
		
		return f;
	}
	
	public String getMessage() {
		return getArguments().getString(ARGS_MESSAGE);
	}
	
	@Override
	protected View safelyOnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) throws Exception {
		WindowManager wm = (WindowManager) TagByApplication.getInstance().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		
		float scalingX = metrics.widthPixels / 2560f;
		float scalingY = metrics.heightPixels / 1504f;
		
		View rootView = inflater.inflate(R.layout.fragment_message, container, false);		
		mMessageView = (TextView)rootView.findViewById(R.id.fragment_message_message);
		
		if (TagByApplication.getInstance().isLandscape()) {
			ViewUtils.scale(scalingX, scalingY, rootView.findViewById(R.id.fragment_message_container));
		}
		
		mMessageView.setText(getMessage());
		
		return rootView;
	}
	
	public void setMessage(String message) {
		Check.Argument.isNotNull(message, "message");
		
		mMessageView.setText(message);
	}
}