package com.tagby.sdk.simpleapp.fragments;

import com.tagby.sdk.app.TagByDocumentFragment;
import com.tagby.sdk.app.TagByOnClickListener;
import com.tagby.sdk.documents.TagByDocument;
import com.tagby.sdk.simpleapp.EventNames;
import com.tagby.sdk.simpleapp.IProvideSendingEvents;
import com.tagby.sdk.simpleapp.R;
import com.tagby.sdk.utils.Check;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ShareSocialTagFragment extends TagByDocumentFragment<TagByDocument> {
	public interface Listener {
		public void onSocialTagClosed() throws Exception;
		public void onSocialTagCancelled() throws Exception;
		public void onConnectFacebook() throws Exception;
		public void onConnectTwitter() throws Exception;
		public void onConnectEmail() throws Exception;
	}
	
	public static ShareSocialTagFragment newInstance (TagByDocument document) {
		Check.Argument.isNotNull(document, "document");
	
		ShareSocialTagFragment fragment = new ShareSocialTagFragment();

		fragment.setArguments(fragment.createBundle(document));
		
		return fragment;
	}
	
	@Override
	protected void safelyOnCreate(Bundle savedInstanceState) throws Exception {
		Log.d(TAG, "safelyOnCreate");
		
		super.safelyOnCreate(savedInstanceState);
		
		Activity activity = getActivity();
		
		if (activity instanceof IProvideSendingEvents) {
			((IProvideSendingEvents)activity).sendEvent(
					EventNames.SHARE_NO_USER_NO_SOCIAL_TAG_SHOWN);
		}
	};
	
	@Override
	protected View safelyOnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) throws Exception {
		final TagByDocument document = getDocument();
		
        View rootView = inflater.inflate(R.layout.fragment_share_social_tag, container, false);
        View close = rootView.findViewById(R.id.fragment_share_social_tag_close);
        View cancel = rootView.findViewById(R.id.fragment_share_social_tag_cancel);
        
        View connectFacebook = rootView.findViewById(R.id.fragment_share_social_tag_facebook);
        View connectTwitter = rootView.findViewById(R.id.fragment_share_social_tag_twitter);
        View connectEmail = rootView.findViewById(R.id.fragment_share_social_tag_email);
        
        boolean facebookSupported = document.getSocialNetworks().getFacebook().isSupported();
        boolean twitterSuppported = document.getSocialNetworks().getTwitter().isSupported();
        boolean emailSuppported = document.getSocialNetworks().getEmail().isSupported();
        
        connectFacebook.setVisibility(facebookSupported ? View.VISIBLE : View.GONE);
        connectTwitter.setVisibility(twitterSuppported ? View.VISIBLE : View.GONE);
        connectEmail.setVisibility(emailSuppported ? View.VISIBLE : View.GONE);        	        
       		
        close.setOnClickListener(
        		new TagByOnClickListener() {			
        			@Override
        			protected void safelyOnClick(View v) throws Exception {
        				((Listener)getActivity()).onSocialTagClosed();		
        			}
        		});
        
        cancel.setOnClickListener(
        		new TagByOnClickListener() {			
        			@Override
        			protected void safelyOnClick(View v) throws Exception {
        				((Listener)getActivity()).onSocialTagCancelled();		
        			}
        		});
       
        connectFacebook.setOnClickListener(
        	new TagByOnClickListener() {				
				@Override
				protected void safelyOnClick(View v) throws Exception {
					((Listener)getActivity()).onConnectFacebook();
				}
			});
        
        connectTwitter.setOnClickListener(
            	new TagByOnClickListener() {				
    				@Override
    				protected void safelyOnClick(View v) throws Exception {
    					((Listener)getActivity()).onConnectTwitter();
    				}
    			});
        
        connectEmail.setOnClickListener(
            	new TagByOnClickListener() {				
    				@Override
    				protected void safelyOnClick(View v) throws Exception {
    					((Listener)getActivity()).onConnectEmail();
    				}
    			});
            
        return rootView;
	}
}