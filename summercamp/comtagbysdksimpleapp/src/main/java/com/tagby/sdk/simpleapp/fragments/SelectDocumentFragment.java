package com.tagby.sdk.simpleapp.fragments;

import it.sephiroth.android.library.widget.AbsHListView;
import it.sephiroth.android.library.widget.HListView;

import org.json.JSONException;

import com.android.volley.toolbox.NetworkImageView;
import com.tagby.sdk.Config;
import com.tagby.sdk.app.MessagePresenter;
import com.tagby.sdk.app.TagByActivity;
import com.tagby.sdk.app.TagByApplication;
import com.tagby.sdk.app.TagByFragment;
import com.tagby.sdk.documents.TagByDocument;
import com.tagby.sdk.simpleapp.R;
import com.tagby.sdk.ui.NetworkImageProgressView;
import com.tagby.sdk.utils.Check;
import com.tagby.sdk.utils.ScaleFactor;
import com.tagby.sdk.utils.ViewUtils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SelectDocumentFragment<T extends TagByDocument> extends TagByFragment {
	
	private BaseAdapter mAdapter;
	private TextView mNoOffersView;
	
	public interface DocumentSelectionListener<T extends TagByDocument> {
		public void onDocumentSelected(T document) throws Exception;
	}
	
	@Override
	protected View safelyOnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		WindowManager wm = (WindowManager) TagByApplication.getInstance().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		
		float scalingX = metrics.widthPixels / 2560f;
		float scalingY = metrics.heightPixels / 1504f;
		
        View rootView = inflater.inflate(R.layout.fragment_select_document, container, false);
        mNoOffersView = (TextView)rootView.findViewById(R.id.fragment_select_document_no_offers_hint);
        ImageView icon = (ImageView)rootView.findViewById(R.id.fragment_select_document_icon); 
        //GridView previews = (GridView)rootView.findViewById(R.id.fragment_select_document_previews);

        HListView previews = (HListView)rootView.findViewById(R.id.fragment_select_document_previews);
        
        boolean isLandscape = TagByApplication.getInstance().isLandscape();
        
        if (isLandscape) {
			ViewUtils.scale(scalingX, scalingY, icon);
			ViewUtils.scale(scalingX, scalingY, previews);
			mAdapter = new HListViewPreviewsLandscapeAdapter(new ScaleFactor(scalingX, scalingY));
        }
        else {
        	mAdapter = new HListViewPreviewsPortraitAdapter();
        }
		
        mNoOffersView = (TextView)rootView.findViewById(R.id.fragment_select_document_no_offers_hint);
        setupNoOffersLabel();
        updateNoOffersVisibility();
        
        //previews.setAdapter(new GridViewPreviewsAdapter());
		previews.setAdapter(mAdapter);
        
        return rootView;
    }
	
	@Override
	protected void safelyOnResume() throws Exception {
		super.safelyOnResume();
		
		((TagByActivity<T>)getActivity()).refreshDocuments();	
	}
	
	public void onDocumentsChanged() {
		mAdapter.notifyDataSetChanged();
		updateNoOffersVisibility();
	}
	
	private void setupNoOffersLabel() {		
		String hint = getString(R.string.fragment_select_document_no_offers_hint);
		String hintUrlPlaceholder = getString(R.string.fragment_select_document_no_offers_hint_url_placeholder);
		
		int placeholderIndex = hint.indexOf(hintUrlPlaceholder);
		
		if (placeholderIndex == -1) {
			mNoOffersView.setText(hint);
		}
		else {
			SpannableStringBuilder ssb = new SpannableStringBuilder();
						
			ssb.append(hint, 0, placeholderIndex);
			int start = ssb.length();
			ssb.append(hintUrlPlaceholder);
			URLSpan span = new URLSpan(Config.getEnvironment().getWebPortal().getWebPortalUrl()) {
				@Override
			    public void updateDrawState(TextPaint ds) {										
					ds.setTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL));
				}
			};
			ssb.setSpan(span, start, ssb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			ssb.append(hint, placeholderIndex + hintUrlPlaceholder.length(), hint.length());
			
			mNoOffersView.setText(ssb);
			mNoOffersView.setMovementMethod(LinkMovementMethod.getInstance());
			mNoOffersView.setLinksClickable(true);
		}
	}
	
	private void updateNoOffersVisibility() {		
		mNoOffersView.setVisibility(mAdapter.getCount() == 0 ? View.VISIBLE : View.GONE);
	}
	
	public class HListViewPreviewsLandscapeAdapter extends BaseAdapter {
		private final ScaleFactor mScaleFactor;
		
		public HListViewPreviewsLandscapeAdapter(ScaleFactor scaleFactor) {
			Check.Argument.isNotNull(scaleFactor, "scaleFactor");
			
			mScaleFactor = scaleFactor;
		}
		
		@Override
		public int getCount() {
			// we group the items in pairs
			return (TagByApplication.getInstance().getDocuments().getDocuments().size() + 1) >> 1;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			int firstDocumentPosition = 2 * position;
			int secondDocumentPosition = firstDocumentPosition + 1;
			
			final T document1 = (T)TagByApplication.getInstance().getDocuments().getDocuments().get(firstDocumentPosition);
			final T document2 = (secondDocumentPosition < TagByApplication.getInstance().getDocuments().getDocuments().size()) ? (T)TagByApplication.getInstance().getDocuments().getDocuments().get(secondDocumentPosition) : null; 
			
			LinearLayout imageContainer = (LinearLayout)convertView;
			NetworkImageProgressView imageView1;
			NetworkImageProgressView imageView2;
			
			if (imageContainer == null) {
				imageContainer = new LinearLayout(getActivity());
				imageContainer.setLayoutParams(new AbsHListView.LayoutParams(800, 2 * 455 + 60));
				imageContainer.setOrientation(LinearLayout.VERTICAL);
				
				LinearLayout image1Container = new LinearLayout(getActivity());
				image1Container.setLayoutParams(new LinearLayout.LayoutParams(800, 455));
				
				imageView1 = new NetworkImageProgressView(getActivity());
				imageView1.setLayoutParams(new LinearLayout.LayoutParams(800, 455));
				imageView1.setScaleType(ScaleType.FIT_XY);
				
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(800, 455);
				lp.setMargins(0, 60, 0, 0);
				imageView2 = new NetworkImageProgressView(getActivity());
				imageView2.setLayoutParams(lp);
				imageView2.setScaleType(ScaleType.FIT_XY);
				
				image1Container.addView(imageView1);
				imageContainer.addView(image1Container);
				imageContainer.addView(imageView2);
			}
			else {
				imageView1 = (NetworkImageProgressView)((LinearLayout)imageContainer.getChildAt(0)).getChildAt(0);
				imageView2 = (NetworkImageProgressView)imageContainer.getChildAt(1);
			}
									
			imageView1.setOnClickListener(new OnClickListener() {				
				@Override
				public void onClick(View v) {
					try {
						((DocumentSelectionListener<T>)getActivity()).onDocumentSelected(document1);
					}
					catch (Exception exception) {
						MessagePresenter.getInstance().showException(exception);
					}
				}
			});
			
			if (document2 == null) {
				imageView2.setVisibility(View.GONE);
			}
			else {
				imageView2.setVisibility(View.VISIBLE);
				
				imageView2.setOnClickListener(new OnClickListener() {				
					@Override
					public void onClick(View v) {
						try {
							((DocumentSelectionListener<T>)getActivity()).onDocumentSelected(document2);
						}
						catch (Exception exception) {
							MessagePresenter.getInstance().showException(exception);
						}
					}
				});
			}
			
			if (document1.getOffer().getPreviewUrl() != null) {
				imageView1.setScaledImageUrl(
					document1.getOffer().getPreviewUrl(),
					TagByApplication.getInstance().getImageLoader(),
					mScaleFactor);
			}
			else if (document1.getOffer().getPreviewResId() != null){
				imageView1.setImageResource(document1.getOffer().getPreviewResId());
			}
			
			if (document2 != null) {
				if (document2.getOffer().getPreviewUrl() != null) {
					imageView2.setScaledImageUrl(
						document2.getOffer().getPreviewUrl(),
						TagByApplication.getInstance().getImageLoader(),
						mScaleFactor);
				}
				else if (document2.getOffer().getPreviewResId() != null){
					imageView2.setImageResource(document2.getOffer().getPreviewResId());
				}
			}
			
			return imageContainer;
		}
	}
	
	public class HListViewPreviewsPortraitAdapter extends BaseAdapter {		
		@Override
		public int getCount() {
			return TagByApplication.getInstance().getDocuments().getDocuments().size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {			
			final T document = (T)TagByApplication.getInstance().getDocuments().getDocuments().get(position); 
			
			NetworkImageProgressView imageView = (NetworkImageProgressView)convertView;
			
			if (imageView == null) {
				imageView = new NetworkImageProgressView(getActivity());
				imageView.setLayoutParams(new AbsHListView.LayoutParams(
						(int)ViewUtils.convertDpToPixel(getActivity(), 247),
						(int)ViewUtils.convertDpToPixel(getActivity(), 389.6f)));
				imageView.setScaleType(ScaleType.FIT_XY);
			}
									
			imageView.setOnClickListener(new OnClickListener() {				
				@Override
				public void onClick(View v) {
					try {
						((DocumentSelectionListener<T>)getActivity()).onDocumentSelected(document);
					}
					catch (Exception exception) {
						MessagePresenter.getInstance().showException(exception);
					}
				}
			});			
			
			if (document.getOffer().getPreviewUrl() != null) {
				imageView.setImageUrl(
					document.getOffer().getPreviewUrl(),
					TagByApplication.getInstance().getImageLoader());
			}
			else if (document.getOffer().getPreviewResId() != null) {
				imageView.setImageResource(document.getOffer().getPreviewResId());
			}
			
			return imageView;
		}
	}
	
	public class GridViewPreviewsAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			return TagByApplication.getInstance().getDocuments().getDocuments().size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final T document = (T)TagByApplication.getInstance().getDocuments().getDocuments().get(position);
			
			NetworkImageView imageView = (NetworkImageView)convertView;
			
			if (imageView == null) {
				imageView = new NetworkImageView(getActivity());
				imageView.setLayoutParams(new GridView.LayoutParams(800, 455));
				imageView.setScaleType(ScaleType.FIT_XY);
			}
						
			imageView.setOnClickListener(new OnClickListener() {				
				@Override
				public void onClick(View v) {
					try {
						((DocumentSelectionListener<T>)getActivity()).onDocumentSelected(document);
					}
					catch (Exception exception) {
						MessagePresenter.getInstance().showException(exception);
					}
				}
			});
			
			Log.d("TEST", position + " " + document.getOffer().getPreviewUrl());
			
			if (document.getOffer().getPreviewUrl() != null) {
				imageView.setImageUrl(document.getOffer().getPreviewUrl(), TagByApplication.getInstance().getImageLoader());
			}
			else if (document.getOffer().getPreviewResId() != null) {
				imageView.setImageResource(document.getOffer().getPreviewResId());
			}
			
			return imageView;
		}
	}
}