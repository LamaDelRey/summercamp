package com.example.noob.mobilesummercamp;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by salve_000 on 02/07/2015.
 */
public class DataHolderProduct {
    static private DataHolderProduct _instance;
    private JSONArray _offres;

    private DataHolderProduct() {
        fetchData();
    }

    private void fetchData() {
        HttpAsyncTask civRequest = new HttpAsyncTask();
        civRequest.execute("http://91.121.192.157:8080/SummerCamp/GetProduct.php");
        try {
            _offres = new JSONArray(civRequest.get());
        } catch (Exception e) {
            System.out.println("error : " + e);
            fetchData();
        }
    }

    static synchronized public DataHolderProduct getInstance() {
        if (_instance == null) {
            _instance = new DataHolderProduct();
        }
        return _instance;
    }

    public JSONObject getProduct(int i){
        try {
            return _offres.getJSONObject(i);
        } catch (JSONException e) {
            return null;
        }
    }

    public JSONArray getProducts() {
        return _offres;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        public String GET(String url) {
            InputStream inputStream = null;
            String result = "";
            try {
                // create HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                // make GET request to the given URL
                HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
                // receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();
                // convert inputstream to string
                if (inputStream != null) {
                    result = convertInputStreamToString(inputStream);
                } else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }
            return result;
        }

        private String convertInputStreamToString(InputStream inputStream) throws IOException {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while ((line = bufferedReader.readLine()) != null)
                result += line;

            inputStream.close();
            return result;

        }
    }
}
