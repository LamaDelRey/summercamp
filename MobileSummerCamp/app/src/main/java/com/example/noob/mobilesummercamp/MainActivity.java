package com.example.noob.mobilesummercamp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONArray;
import org.json.JSONObject;


public class MainActivity extends Activity {

    TextView t;
    private int mProgressStatus = 0;
    private int offreID = 0;
    private int jsonID = 0;
    public static final String PREFS_NAME = "MyPrefsFile";
    ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Intent intent = getIntent();
        offreID = intent.getIntExtra("EXTRA_INT", 0);
        jsonID = intent.getIntExtra("JSON_ID", 0);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        JSONObject offre;
        TextView nomoffre;

        try {
            nomoffre = (TextView) findViewById(R.id.nomOffre);
            offre = DataHolder.getInstance().getOffre(jsonID);
            String nomOffre = offre.getString("nomoffre");
            nomoffre.setText(nomOffre);
            Typeface myCustomFont=Typeface.createFromAsset(getAssets(), "fonts/pizzahutfont.ttf");
            nomoffre.setTypeface(myCustomFont);

            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            NetworkImageView thumbNail = (NetworkImageView) findViewById(R.id.imageView2);
            thumbNail.setImageUrl("http://blurmatch.com:8080/SummerCamp/images/" + offre.getString("imageoffre"), imageLoader);

            SharedPreferences sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
            if(sharedPreferences.contains("offreID") && offreID == sharedPreferences.getInt("offreID",0)){
                System.out.println("offreID found!! " + sharedPreferences.getInt("offreID", 0));
                if(sharedPreferences.contains("progressMade"))
                    mProgressStatus = sharedPreferences.getInt("progressMade",0);
            }
            else if (sharedPreferences.contains("offreID")){
                System.out.println("offreID found but you wanna change dontcha? New : "+ offreID + "Old : " + sharedPreferences.getInt("offreID",0));
                sharedPreferences.edit().putInt("offreID", offreID);
                if(sharedPreferences.contains("progressMade"))
                    mProgressStatus = sharedPreferences.getInt("progressMade",0);
                sharedPreferences.edit().putInt("progressMade",0);
                sharedPreferences.edit().commit();
            }
            else{
                System.out.println("No offreID yet");
                sharedPreferences.edit().putInt("offreID", offreID);
                sharedPreferences.edit().commit();
            }

            final int max = offre.getInt("valeuroffre");
            ProgressBar mProgress = (ProgressBar) findViewById(R.id.progressBar);
            mProgress.setProgress(mProgressStatus);
            mProgress.setMax(max);

    //        System.out.println(offreID);
            t= (TextView) findViewById(R.id.my_text);
            Typeface myCustomFont2 = Typeface.createFromAsset(getAssets(), "fonts/pizzahutfont.ttf");
            t.setTypeface(myCustomFont2);
    //        toast.show();
            if (mProgressStatus >= max){
                t.setText("Cadeau a retirer!");
            }
            t.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    JSONArray productsArray = DataHolderProduct.getInstance().getProducts();
                    Context context = getApplicationContext();
                    CharSequence text = "Votre total de points augmente!";
                    int duration = Toast.LENGTH_SHORT;
                    ProgressBar mProgress = (ProgressBar) findViewById(R.id.progressBar);
                    if(mProgressStatus >= max){
                        JSONArray  offres;
                        offres = DataHolder.getInstance().getOffres();
                        System.out.println(offres);
                        Toast toast = Toast.makeText(context, "Presentez-vous a un vendeur pour obtenir votre cadeau!", Toast.LENGTH_LONG);
                        toast.show();
                    }
                    else{
                        TextView code = (TextView) findViewById(R.id.editText);
                        if(code.getText() != null){
                            int len = productsArray.length();
                            CharSequence codeJSON = code.getText();
                            String codeStringed = codeJSON.toString();
                            int indexCode = -1;
                            for (int i = 0 ; i < len ; i++ ){
                                try {
                                    JSONObject product = productsArray.getJSONObject(i);
                                    System.out.println("Case : "+i+" resultat d'Equals " + codeStringed.equals(product.getString("Code")));
                                    if(codeStringed.equals(product.getString("Code"))) {
                                        indexCode = i;
                                    }
                                }
                                catch(Exception e){
                                    System.out.println("Error : " + e);
                                }
                            }

                            if(indexCode == -1){
                                Toast toast = Toast.makeText(context, "Code invalide!", duration);
                                toast.show();
                            }
                            else{
                                try{
                                    JSONObject usedProduct = productsArray.getJSONObject(indexCode);
                                    int zebi = usedProduct.getInt("Valeur");
                                    mProgressStatus += zebi;
                                    mProgress.setProgress(mProgressStatus);
                                    Toast toast = Toast.makeText(context, text, duration);
                                    toast.show();
                                }
                                catch(Exception e){
                                    System.out.println("Error : " + e);
                                }
                            }

                        }
                        else{
                            Toast toast = Toast.makeText(context, "Vous devez rentrer un code pour gagner des points", duration);
                            toast.show();
                        }

                    }
                }
            });
        }
        catch (Exception e) {
            System.out.println("Error : " + e);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity2, menu);
        return true;
    }

    @Override
    protected void onStop(){
        super.onStop();

        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("offreID", offreID);
        editor.putInt("progressMade", mProgressStatus);
        // Commit the edits!
        editor.commit();
    }

}
