package com.example.noob.mobilesummercamp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class ListOffer extends Activity {

    private static final String TAG = ListOffer.class.getSimpleName();
    private static final String url = "http://91.121.192.157:8080/SummerCamp/GetOffre.php";
    private ProgressDialog pDialog;
    private List<Offer> offerList = new ArrayList<>();
    private ListView listView;
    private CustomOfferAdapter adapter;
    private int IDJSON;
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_offer);
        listView = (ListView) findViewById(R.id.listoffer);
        adapter = new CustomOfferAdapter(this, offerList);
        listView.setAdapter(adapter);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        JsonArrayRequest offerReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        hidePDialog();

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Offer offer = new Offer();
                                IDJSON =  obj.getInt("id");
                                offer.setId(Integer.parseInt(obj.getString("id")));
                                offer.setThumbnailUrl("http://blurmatch.com:8080/SummerCamp/images/" + obj.getString("imageoffre"));
                                offer.setTitle(new String(obj.getString("nomoffre").getBytes("ISO-8859-1"), "UTF-8"));
                                offer.setDescription(new String(obj.getString("description").getBytes("ISO-8859-1"), "UTF-8"));
                             //   offer.setStartoffre(JSONfunctions.parse(obj.getString("startoffre")));
                             //   offer.setFinoffre(JSONfunctions.parse(obj.getString("finoffre")));
                                offer.setValue(Integer.parseInt(obj.getString("valeuroffre")));
                                offerList.add(offer);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
            }
        });

        AppController.getInstance().addToRequestQueue(offerReq);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                int itemPosition = position;
                final int idJSON = IDJSON;
                Offer itemValue = (Offer) listView.getItemAtPosition(position);

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("offerDetails", position);
                intent.putExtra("EXTRA_INT", idJSON);
                intent.putExtra("JSON_ID",itemPosition);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }
}
