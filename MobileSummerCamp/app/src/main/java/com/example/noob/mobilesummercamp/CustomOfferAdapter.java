package com.example.noob.mobilesummercamp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

/**
 * Created by Raphael on 02/07/2015.
 */
public class CustomOfferAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Offer> offerItems;
    ImageLoader imageLoader;

    public CustomOfferAdapter(Activity activity, List<Offer> offerItems) {
        this.activity = activity;
        this.offerItems = offerItems;
    }

    @Override
    public int getCount() {
        return offerItems.size();
    }

    @Override
    public Object getItem(int location) {
        return offerItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_offer, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        NetworkImageView thumbNail = (NetworkImageView) convertView
                .findViewById(R.id.thumbnail);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView description = (TextView) convertView.findViewById(R.id.description);

        Offer r = offerItems.get(position);

        thumbNail.setImageUrl(r.getThumbnailUrl(), imageLoader);

        title.setText(r.getTitle());

//        String[] strArr = r.getDescription().split("\\s+", 8);

 //       StringBuilder strBuilder = new StringBuilder();

//        for (int i = 0; i < 7; i++) {
   //         strBuilder.append(" " + strArr[i]);
    //    }

        String newString = r.getDescription();

        description.setText(newString);

        return convertView;
    }
}
