package com.example.noob.mobilesummercamp;

import java.util.Date;

/**
 * Created by Raphael on 02/07/2015.
 */
public class Offer {

    private int id;
    private String title;
    private String description;
    private int value;
    private Date startoffre;
    private Date finoffre;
    private String thumbnailUrl;

    public Offer() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Date getFinoffre() {
        return finoffre;
    }

    public void setFinoffre(Date finoffre) {
        this.finoffre = finoffre;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public Date getStartoffre() {
        return startoffre;
    }

    public void setStartoffre(Date startoffre) {
        this.startoffre = startoffre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
